import * as description from './chart.md';
import examples from './examples';

export default {
  description,
  examples,
};
