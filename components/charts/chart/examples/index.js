import ChartBasicExample from './chart.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'chart-basic',
        name: 'Basic',
        description: 'Basic Chart',
        component: ChartBasicExample,
      },
    ],
  },
];
