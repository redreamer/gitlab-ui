import AreaBasicExample from './area.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'area-basic',
        name: 'Basic',
        description: 'Basic Area Chart',
        component: AreaBasicExample,
      },
    ],
  },
];
