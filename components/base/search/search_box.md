# Search Box

Provides a clearable text input with loading state to be used for [search by typing](https://design.gitlab.com/components/search).
