## Additional notes

> Note: The limits prop is used to define pagination link limits based on Bootstrap's breakpoint sizes. It is strongly recommended you provide a 'default' property, even if you have accounted for all breakpoint sizes. While unlikely, it is possible breakpoints could change, thus, a default property ensures a graceful fallback. 

> Note: The component will not render any UI if the total items available for display is less than the max items per page.
